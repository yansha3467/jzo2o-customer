package com.jzo2o.customer.controller.worker;

import com.jzo2o.customer.model.dto.request.WorkerCertificationAuditAddReqDTO;
import com.jzo2o.customer.model.dto.response.RejectReasonResDTO;
import com.jzo2o.customer.service.IWorkerCertificationAuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("workerCertificationController")
@RequestMapping("/worker/worker-certification-audit")
@Api(tags = "服务端-提交认证接口")
public class WorkerCertificationController {
    @Autowired
    private IWorkerCertificationAuditService workerCertificationAuditService;
    @ApiOperation("服务端提交认证申请")
    @PostMapping
    public void addWorkerCertification(@RequestBody WorkerCertificationAuditAddReqDTO workerCertificationAuditAddReqDTO){
        workerCertificationAuditService.addWorkerCertification(workerCertificationAuditAddReqDTO);
    }
    @ApiOperation("查询最新的驳回原因")
    @GetMapping("/rejectReason")
    public RejectReasonResDTO rejectReason(){
       return workerCertificationAuditService.rejectReason();
    }

}

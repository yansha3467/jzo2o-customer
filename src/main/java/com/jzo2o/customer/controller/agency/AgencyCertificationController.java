package com.jzo2o.customer.controller.agency;

import com.jzo2o.customer.model.dto.request.AgencyCertificationAuditAddReqDTO;
import com.jzo2o.customer.model.dto.response.AgencyCertificationAuditResDTO;
import com.jzo2o.customer.service.IAgencyCertificationAuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("agencyAgencyCertificationController")
@RequestMapping("/agency/agency-certification-audit")
@Api(tags = "机构端-提交认证接口")
public class AgencyCertificationController {
    @Autowired
private IAgencyCertificationAuditService agencyCertificationAuditService;
    @PostMapping
    @ApiOperation("机构提交认证申请")
    public void addAgencyCertification(@RequestBody AgencyCertificationAuditAddReqDTO agencyCertificationAuditAddReqDTO){
agencyCertificationAuditService.addAgencyCertification(agencyCertificationAuditAddReqDTO);
    }
}

package com.jzo2o.customer.controller.agency;

import cn.hutool.core.bean.BeanUtil;
import com.jzo2o.common.utils.BeanUtils;
import com.jzo2o.customer.model.domain.BankAccount;
import com.jzo2o.customer.model.dto.request.BankAccountUpsertReqDTO;
import com.jzo2o.customer.model.dto.response.BankAccountResDTO;
import com.jzo2o.customer.service.BankAccountService;
import com.jzo2o.mvc.utils.UserContext;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController("agencyBankAccountController")
@RequestMapping("/agency/bank-account")
@Api(tags = "机构端 - 设置银行账户接口")
public class BankAccountController {
    @Autowired
    private BankAccountService bankAccountService;

    @PostMapping
    @ApiOperation("新增或更新银行账号信息")
    public void addOrupdateBankAccount(@RequestBody BankAccountUpsertReqDTO bankAccountUpsertReqDTO) {

        bankAccountService.addOrupdateBankAccount(bankAccountUpsertReqDTO);
    }
    @GetMapping("/currentUserBankAccount")
    @ApiOperation("获取当前用户银行账号接口")
    public BankAccountResDTO currentUserBankAccount() {
        Long userId = UserContext.currentUserId();
        BankAccount bankAccount = bankAccountService.getById(userId);
       return BeanUtil.toBean(bankAccount, BankAccountResDTO.class);
    }
}

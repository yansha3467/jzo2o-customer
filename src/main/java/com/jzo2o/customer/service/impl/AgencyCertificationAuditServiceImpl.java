package com.jzo2o.customer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jzo2o.common.model.PageResult;
import com.jzo2o.customer.enums.CertificationStatusEnum;
import com.jzo2o.customer.mapper.AgencyCertificationAuditMapper;
import com.jzo2o.customer.model.domain.AgencyCertification;
import com.jzo2o.customer.model.domain.AgencyCertificationAudit;
import com.jzo2o.customer.model.dto.request.AgencyCertificationAuditAddReqDTO;
import com.jzo2o.customer.model.dto.request.AgencyCertificationAuditPageQueryReqDTO;
import com.jzo2o.customer.model.dto.response.AgencyCertificationAuditResDTO;
import com.jzo2o.customer.service.IAgencyCertificationAuditService;
import com.jzo2o.customer.service.IAgencyCertificationService;
import com.jzo2o.mvc.utils.UserContext;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class AgencyCertificationAuditServiceImpl  extends ServiceImpl<AgencyCertificationAuditMapper, AgencyCertificationAudit>implements IAgencyCertificationAuditService {
    @Resource
    private IAgencyCertificationService agencyCertificationService;
    @Override
    public void addAgencyCertification(AgencyCertificationAuditAddReqDTO agencyCertificationAuditAddReqDTO) {
        //1.新增申请资质认证记录
        AgencyCertificationAudit agencyCertificationAudit = BeanUtil.toBean(agencyCertificationAuditAddReqDTO, AgencyCertificationAudit.class);
        AgencyCertification agencyCertification = agencyCertificationService.getById(agencyCertificationAuditAddReqDTO.getServeProviderId());
        //查询认证记录
        if (ObjectUtil.isNotNull(agencyCertification)){
            //2.将认证信息状态更新为认证中
            agencyCertification.setCertificationStatus(CertificationStatusEnum.PROGRESSING.getStatus());
            agencyCertificationService.updateById(agencyCertification);
        }else{
             agencyCertification= new AgencyCertification();
             agencyCertification.setId(agencyCertificationAuditAddReqDTO.getServeProviderId());
            agencyCertification.setCertificationStatus(CertificationStatusEnum.PROGRESSING.getStatus());
agencyCertificationService.save(agencyCertification);
        }

    }
}

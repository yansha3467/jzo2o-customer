package com.jzo2o.customer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jzo2o.common.model.PageResult;
import com.jzo2o.customer.enums.CertificationStatusEnum;
import com.jzo2o.customer.mapper.WorkerCertificationAuditMapper;
import com.jzo2o.customer.mapper.WorkerCertificationMapper;
import com.jzo2o.customer.model.domain.WorkerCertification;
import com.jzo2o.customer.model.domain.WorkerCertificationAudit;
import com.jzo2o.customer.model.dto.request.AgencyCertificationAuditPageQueryReqDTO;
import com.jzo2o.customer.model.dto.request.CertificationAuditReqDTO;
import com.jzo2o.customer.model.dto.request.WorkerCertificationAuditAddReqDTO;
import com.jzo2o.customer.model.dto.request.WorkerCertificationAuditPageQueryReqDTO;
import com.jzo2o.customer.model.dto.response.RejectReasonResDTO;
import com.jzo2o.customer.model.dto.response.WorkerCertificationAuditResDTO;
import com.jzo2o.customer.service.IWorkerCertificationAuditService;
import com.jzo2o.customer.service.IWorkerCertificationService;
import com.jzo2o.mvc.utils.UserContext;
import com.jzo2o.mysql.utils.PageUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class IWorkerCertificationAuditServiceImpl extends ServiceImpl<WorkerCertificationAuditMapper, WorkerCertificationAudit> implements IWorkerCertificationAuditService {
    @Resource
    private IWorkerCertificationService workerCertificationService;

    /**
     * 服务端提交认证申请
     *
     * @param workerCertificationAuditAddReqDTO
     */
    @Override
    public void addWorkerCertification(WorkerCertificationAuditAddReqDTO workerCertificationAuditAddReqDTO) {
        Long userId = UserContext.currentUserId();
        workerCertificationAuditAddReqDTO.setServeProviderId(userId);
        WorkerCertificationAudit workerCertificationAudit = BeanUtil.toBean(workerCertificationAuditAddReqDTO, WorkerCertificationAudit.class);
        workerCertificationAudit.setAuditStatus(0);
        baseMapper.insert(workerCertificationAudit);
        //查询认证状态
        Long serveProviderId = workerCertificationAuditAddReqDTO.getServeProviderId();
        WorkerCertification workerCertification = workerCertificationService.getById(serveProviderId);
        if (ObjectUtil.isNotNull(workerCertification)) {
            //2.将认证信息状态更新为认证中
            workerCertification.setCertificationStatus(CertificationStatusEnum.PROGRESSING.getStatus());
            workerCertificationService.updateById(workerCertification);
        } else {
            workerCertification = new WorkerCertification();
            workerCertification.setId(userId);
            workerCertification.setCertificationStatus(CertificationStatusEnum.PROGRESSING.getStatus());//认证中
            workerCertificationService.save(workerCertification);
        }
    }

    /**
     * 查询最新的驳回原因
     *
     * @return
     */
    @Override
    public RejectReasonResDTO rejectReason() {
        LambdaQueryWrapper<WorkerCertificationAudit> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(WorkerCertificationAudit::getServeProviderId, UserContext.currentUserId()).orderByDesc(WorkerCertificationAudit::getCreateTime).last("limit 1");
        WorkerCertificationAudit workerCertificationAudit = baseMapper.selectOne(wrapper);
        String rejectReason = workerCertificationAudit.getRejectReason();
        RejectReasonResDTO rejectReasonResDTO = new RejectReasonResDTO();
        rejectReasonResDTO.setRejectReason(rejectReason);
        return rejectReasonResDTO;
    }

    /**
     * 服务人员认证审核信息分页查询
     *
     * @param workerCertificationAuditPageQueryReqDTO
     * @return
     */
    @Override
    public PageResult<WorkerCertificationAuditResDTO> pageQuery(WorkerCertificationAuditPageQueryReqDTO workerCertificationAuditPageQueryReqDTO) {
        Page<WorkerCertificationAudit> page = PageUtils.parsePageQuery(workerCertificationAuditPageQueryReqDTO, WorkerCertificationAudit.class);
        QueryWrapper<WorkerCertificationAudit> wrapper = new QueryWrapper<>();
        wrapper.like("name", workerCertificationAuditPageQueryReqDTO.getName())
                .eq("audit_status",workerCertificationAuditPageQueryReqDTO.getAuditStatus())
                .eq("certification_status",workerCertificationAuditPageQueryReqDTO.getCertificationStatus())
                .eq("id_card_no",workerCertificationAuditPageQueryReqDTO.getIdCardNo());
        Page<WorkerCertificationAudit> workerCertificationAuditPage = baseMapper.selectPage(page, wrapper);

        return PageUtils.toPage(workerCertificationAuditPage, WorkerCertificationAuditResDTO.class);
    }

    /**
     * 审核服务人员认证信息
     *
     * @param id
     * @param certificationAuditReqDTO
     */
    @Override
    public void auditCertification(Long id, CertificationAuditReqDTO certificationAuditReqDTO) {
        Long userId = UserContext.currentUserId();
        
    }
}

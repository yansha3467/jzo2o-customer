package com.jzo2o.customer.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.update.LambdaUpdateChainWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jzo2o.api.customer.dto.response.AddressBookResDTO;
import com.jzo2o.api.publics.MapApi;
import com.jzo2o.api.publics.dto.response.LocationResDTO;
import com.jzo2o.common.enums.EnableStatusEnum;
import com.jzo2o.common.expcetions.CommonException;
import com.jzo2o.common.expcetions.ForbiddenOperationException;
import com.jzo2o.common.model.PageResult;
import com.jzo2o.common.utils.BeanUtils;
import com.jzo2o.common.utils.CollUtils;
import com.jzo2o.common.utils.NumberUtils;
import com.jzo2o.common.utils.StringUtils;
import com.jzo2o.customer.mapper.AddressBookMapper;
import com.jzo2o.customer.mapper.CommonUserMapper;
import com.jzo2o.customer.model.domain.AddressBook;
import com.jzo2o.customer.model.dto.request.AddressBookPageQueryReqDTO;
import com.jzo2o.customer.model.dto.request.AddressBookUpsertReqDTO;
import com.jzo2o.customer.service.IAddressBookService;
import com.jzo2o.mvc.utils.UserContext;
import com.jzo2o.mysql.utils.PageUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 地址薄 服务实现类
 * </p>
 *
 * @author itcast
 * @since 2023-07-06
 */
@Service
public class AddressBookServiceImpl extends ServiceImpl<AddressBookMapper, AddressBook> implements IAddressBookService {
    @Resource
    private AddressBookMapper addressBookMapper;
    @Resource
    private MapApi mapApi;
    @Override
    public List<AddressBookResDTO> getByUserIdAndCity(Long userId, String city) {

        List<AddressBook> addressBooks = lambdaQuery()
                .eq(AddressBook::getUserId, userId)
                .eq(AddressBook::getCity, city)
                .list();
        if (CollUtils.isEmpty(addressBooks)) {
            return new ArrayList<>();
        }
        return BeanUtils.copyToList(addressBooks, AddressBookResDTO.class);
    }

    /**
     * 取消默认
     *
     * @param userId 用户id
     */
    private void cancelDefault(Long userId) {
        LambdaUpdateWrapper<AddressBook> updateWrapper = Wrappers.<AddressBook>lambdaUpdate()
                .eq(AddressBook::getUserId, userId)
                .set(AddressBook::getIsDefault, 0);
        super.update(updateWrapper);
    }


    /**
     * 新增地址簿
     *
     * @param addressBookUpsertReqDTO
     */
    @Override
    public void add(AddressBookUpsertReqDTO addressBookUpsertReqDTO) {
        long currentUserId = UserContext.currentUserId();
        if (1 == addressBookUpsertReqDTO.getIsDefault()) {
            cancelDefault(currentUserId);
        }
        AddressBook book = BeanUtil.toBean(addressBookUpsertReqDTO, AddressBook.class);
        book.setUserId(currentUserId);
        String location = addressBookUpsertReqDTO.getLocation();
        System.out.println("location" + location);
        String[] split = location.split(",");
        for (int i = 0; i < split.length; i++) {
            System.out.println(split[i]);
            book.setLon(Double.valueOf(split[0]));
            book.setLat(Double.valueOf(split[1]));

        }
        baseMapper.insert(book);
    }

    /**
     * 地址薄分页查询接口
     *
     * @param addressBookPageQueryReqDTO
     * @return
     */
    @Override
    public PageResult<AddressBookResDTO> page(AddressBookPageQueryReqDTO addressBookPageQueryReqDTO) {
        Long userId = UserContext.currentUserId();
        Page<AddressBook> page = PageUtils.parsePageQuery(addressBookPageQueryReqDTO, AddressBook.class);
        LambdaQueryWrapper<AddressBook> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(AddressBook::getUserId, userId);
        Page<AddressBook> addressBookPage = baseMapper.selectPage(page, wrapper);
        return PageUtils.toPage(addressBookPage, AddressBookResDTO.class);

    }



   /**
     * 地址薄修改接口
     *
     * @param id
     * @param addressBookUpsertReqDTO
     */
    @Override
    public void updateaddressbook(Long id, AddressBookUpsertReqDTO addressBookUpsertReqDTO) {
        if (1 == addressBookUpsertReqDTO.getIsDefault()) {
            cancelDefault(UserContext.currentUserId());
        }
        AddressBook book = BeanUtil.toBean(addressBookUpsertReqDTO, AddressBook.class);
        book.setId(id);
        String address= addressBookUpsertReqDTO.getProvince() + addressBookUpsertReqDTO.getCity() + addressBookUpsertReqDTO.getAddress() + addressBookUpsertReqDTO.getCounty();
        LocationResDTO locationResDTO = mapApi.getLocationByAddress(address);
        String location = locationResDTO.getLocation();
       if (StringUtils.isNotEmpty(location)){
           book.setLon(NumberUtils.parseDouble(location.split(",")[0]));
           book.setLat(NumberUtils.parseDouble(location.split(",")[1]));
       }
        baseMapper.updateById(book);
    }

    /**
     * 设置/取消默认地址
     *
     * @param userId
     * @param id
     * @param flag
     */
    @Override
    public void updateDefaultStatus(Long userId, Long id, Integer flag) {
        if (1==flag){
            cancelDefault(userId);
        }
        AddressBook addressBook = new AddressBook();
        addressBook.setId(id);
        addressBook.setIsDefault(flag);
        baseMapper.updateById(addressBook);

    }

    /**
     * 获取默认地址接口
     *
     * @return
     */
    @Override
    public AddressBookResDTO defaultAddress() {
        Long userId = UserContext.currentUserId();
        QueryWrapper<AddressBook> wrapper = new QueryWrapper<>();
        wrapper.eq("user_id",userId);
        wrapper.eq("is_default",1);
        AddressBook addressBook = baseMapper.selectOne(wrapper);
        return BeanUtils.toBean(addressBook, AddressBookResDTO.class);
    }
}

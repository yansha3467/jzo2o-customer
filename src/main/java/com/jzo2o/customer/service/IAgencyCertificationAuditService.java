package com.jzo2o.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jzo2o.customer.model.domain.AgencyCertificationAudit;
import com.jzo2o.customer.model.dto.request.AgencyCertificationAuditAddReqDTO;

public interface IAgencyCertificationAuditService extends IService<AgencyCertificationAudit> {
    void addAgencyCertification(AgencyCertificationAuditAddReqDTO agencyCertificationAuditAddReqDTO);
}

package com.jzo2o.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jzo2o.customer.model.domain.BankAccount;
import com.jzo2o.customer.model.dto.request.BankAccountUpsertReqDTO;

public interface BankAccountService  extends IService<BankAccount> {
    /**
     *
     * @param bankAccountUpsertReqDTO
     */
    void addOrupdateBankAccount(BankAccountUpsertReqDTO bankAccountUpsertReqDTO);
}

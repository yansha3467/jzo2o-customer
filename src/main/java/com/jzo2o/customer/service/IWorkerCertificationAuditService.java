package com.jzo2o.customer.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jzo2o.common.model.PageResult;
import com.jzo2o.customer.model.domain.WorkerCertificationAudit;
import com.jzo2o.customer.model.dto.request.AgencyCertificationAuditPageQueryReqDTO;
import com.jzo2o.customer.model.dto.request.CertificationAuditReqDTO;
import com.jzo2o.customer.model.dto.request.WorkerCertificationAuditAddReqDTO;
import com.jzo2o.customer.model.dto.request.WorkerCertificationAuditPageQueryReqDTO;
import com.jzo2o.customer.model.dto.response.RejectReasonResDTO;
import com.jzo2o.customer.model.dto.response.WorkerCertificationAuditResDTO;

public interface IWorkerCertificationAuditService  extends IService<WorkerCertificationAudit> {
    /**
     * 服务端提交认证申请
     * @param workerCertificationAuditAddReqDTO
     */
    void addWorkerCertification(WorkerCertificationAuditAddReqDTO workerCertificationAuditAddReqDTO);

    /**
     * 查询最新的驳回原因
     * @return
     */
    RejectReasonResDTO rejectReason();

    /**
     * 服务人员认证审核信息分页查询
     * @param workerCertificationAuditPageQueryReqDTO
     * @return
     */
    PageResult<WorkerCertificationAuditResDTO> pageQuery(WorkerCertificationAuditPageQueryReqDTO workerCertificationAuditPageQueryReqDTO);

    /**
     * 审核服务人员认证信息
     * @param id
     * @param certificationAuditReqDTO
     */
    void auditCertification(Long id, CertificationAuditReqDTO certificationAuditReqDTO);
}

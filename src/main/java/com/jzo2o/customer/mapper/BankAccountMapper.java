package com.jzo2o.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jzo2o.customer.model.domain.BankAccount;

/**
 * 银行账户Mapper接口
 */
public interface BankAccountMapper extends BaseMapper<BankAccount> {
}

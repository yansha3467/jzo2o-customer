package com.jzo2o.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jzo2o.customer.model.domain.AgencyCertificationAudit;

public interface AgencyCertificationAuditMapper extends BaseMapper<AgencyCertificationAudit> {
}
